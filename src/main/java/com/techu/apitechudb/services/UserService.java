package com.techu.apitechudb.services;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired

    UserRepository userRepository;
    public List<UserModel> findAll(String orderBy) {
        System.out.println("findAll en  User Service");
        List <UserModel> result;
        if(orderBy!=null){
            result=this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
        }
        else {
            result = this.userRepository.findAll();
        }
        return result;
    }

    public Optional<UserModel> findById(String Id) {
        System.out.println("findById en  UserService");
        return this.userRepository.findById(Id);
    }
    public UserModel addUSer(UserModel user){
        System.out.println("addUser en  UserService");
        return this.userRepository.save(user);
    }
    public UserModel updateUser(UserModel user){
        System.out.println("updateUSer en  UserService");
        return this.userRepository.save(user);
    }
    public boolean delete(String Id){
        System.out.println("delete en  UserService");
        boolean result=false;
        if(this.findById(Id).isPresent()==true) {
            System.out.println("usuario encontrado borrando");
            this.userRepository.deleteById(Id);
            result=true;
        }
        return result;
    }
}
