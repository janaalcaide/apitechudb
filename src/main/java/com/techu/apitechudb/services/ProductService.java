package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    public List<ProductModel> findAll() {
        System.out.println("findAll en  ProductService");
        return this.productRepository.findAll();
    }
    public Optional<ProductModel> findById(String Id) {
        System.out.println("findById en  ProductService");
        return this.productRepository.findById(Id);
    }
    public ProductModel addProduct(ProductModel product){
        System.out.println("addProduct en  ProductService");
        return this.productRepository.save(product);
    }
    public ProductModel updateProduct(ProductModel product){
        System.out.println("updateProduct en  ProductService");
        return this.productRepository.save(product);
    }
    public boolean delete(String Id){
        System.out.println("delete en  ProductService");
        boolean result=false;
        if(this.findById(Id).isPresent()==true) {
            System.out.println("Producto encontrado borrando");
            this.productRepository.deleteById(Id);
            result=true;
        }
        return result;
    }
}
