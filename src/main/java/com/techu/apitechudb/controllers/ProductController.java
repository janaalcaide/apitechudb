package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {
    @Autowired
    ProductService  productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProductsby id");
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/products/{id}")
    public  ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProducts");
        System.out.println("la id del producto es " +  id);
        Optional<ProductModel>  result= this.productService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "producto no encontrado",
                result.isPresent() ? HttpStatus.OK: HttpStatus.NOT_FOUND);
    }




    @PostMapping("/products")
    public  ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addproduct");
        System.out.println("la id del producto es " +  product.getId());
        System.out.println("la descripción del producto es " +  product.getDesc());
        System.out.println("el precio del producto es " +  product.getPrice());
        return  new ResponseEntity<>(this.productService.addProduct(product), HttpStatus.CREATED);
    }
    @PutMapping  ("/products/{id}")
    public  ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable  String id){
        System.out.println("updateProduct");
        System.out.println("la id del producto  a actualizar en parámetro URL es " +  id);
        System.out.println("la id del producto  a actualizar es " +  product.getId());
        System.out.println("la descripción del producto a actualizar es " +  product.getDesc());
        System.out.println("el precio del producto a actualizar es  " +  product.getPrice());
        Optional<ProductModel>  productToUpdate= this.productService.findById(id);
        if (productToUpdate.isPresent()){
            System.out.println("Producto encontrado  ");
            this.productService.updateProduct(product);
        }
        return  new ResponseEntity<>(product,productToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @DeleteMapping  ("/products/{id}")
    public  ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProducts");
        System.out.println("la id del producto a borrar es " +  id);
        boolean deletedProduct=this.productService.delete(id);

       return new ResponseEntity<>(deletedProduct  ? "producto borrado": "producto no borrado",
               deletedProduct  ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    }

