package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam (name = "$orderby", required=false) String orderBy){
        System.out.println("getUsers ");
        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }
    @PostMapping("/users")
    public  ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("adduser");
        System.out.println("la id del user es " +  user.getId());
        System.out.println("el name del usuario es " +  user.getName());
        System.out.println("la edad del usuario es " +  user.getAge());
        return  new ResponseEntity<>(this.userService.addUSer(user), HttpStatus.CREATED);
    }
    @GetMapping("/users/{id}")
    public  ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUser");
        System.out.println("la id del usuario es " +  id);
        Optional<UserModel> result= this.userService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "usuario no encontrado",
                result.isPresent() ? HttpStatus.OK: HttpStatus.NOT_FOUND);
    }
    @PutMapping  ("/users/{id}")
    public  ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable  String id){
        System.out.println("updateUSer");
        System.out.println("la id del usuario a actualizar en parámetro URL es " +  id);
        System.out.println("la id del usuario  a actualizar es " +  user.getId());
        System.out.println("el nombre del usuario a actualizar es " +  user.getName());
        System.out.println("la edad del usuario a actualizar es " +  user.getAge());
        Optional<UserModel>  userToUpdate= this.userService.findById(id);
        if (userToUpdate.isPresent()){
            System.out.println("Usuario encontrado  ");
            this.userService.updateUser(user);
        }
        return  new ResponseEntity<>(user,userToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @DeleteMapping  ("/users/{id}")
    public  ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUSer");
        System.out.println("la id del usuario a borrar es " +  id);
        boolean deletedUser=this.userService.delete(id);

        return new ResponseEntity<>(deletedUser  ? "usuario borrado": "usuario no borrado",
                deletedUser  ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
